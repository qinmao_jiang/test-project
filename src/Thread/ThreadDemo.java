package Thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-06-11 15:16:00
 */
public class ThreadDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyThread myThread1 = new MyThread();
        MyThread myThread2 = new MyThread();
        myThread1.setName("线程1");
        myThread2.setName("线程2");
        myThread1.start();
        myThread2.start();

        /*MyRun myRun = new MyRun();
        Thread thread1 = new Thread(myRun);
        Thread thread2 = new Thread(myRun);
        thread1.setName("线程1");
        thread2.setName("线程2");
        thread1.start();
        thread2.start();*/

        /*MyCallable myCallable = new MyCallable();
        FutureTask<Integer> futureTask = new FutureTask<>(myCallable);
        Thread thread1 = new Thread(futureTask);
        thread1.start();
        Integer result = futureTask.get();
        System.out.println(result);*/
    }
}
