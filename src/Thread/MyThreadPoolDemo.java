package Thread;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-06-11 20:45:57
 */
public class MyThreadPoolDemo {
    public static void main(String[] args) {
//        ExecutorService pool = Executors.newCachedThreadPool();
        /*ExecutorService pool = Executors.newFixedThreadPool(3);

        pool.submit(new MyRun());
        pool.submit(new MyRun());
        pool.submit(new MyRun());
        pool.submit(new MyRun());
        pool.submit(new MyRun());

        pool.shutdown();*/

        ThreadPoolExecutor pool = new ThreadPoolExecutor(
                3,
                6,
                60,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );

        pool.submit(new MyRun());
        pool.execute(new MyThread());
        pool.shutdown();
    }
}
