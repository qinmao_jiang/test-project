package Thread;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-06-11 15:21:59
 */
public class MyRun implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            Thread thread = Thread.currentThread();
            System.out.println(thread.getName() + "多线程~" + i);
        }
    }
}
