package Thread;

import java.util.concurrent.Callable;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-06-11 15:30:14
 */
public class MyCallable implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }
        return sum;
    }
}
