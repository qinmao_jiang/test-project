package Thread;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-06-11 15:15:01
 */
public class MyThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getName() + "~多线程~" + i);
        }
    }
}
