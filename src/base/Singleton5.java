package base;

import java.io.Serializable;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-05-05 15:43:09
 */

//单例模式 - 内部类懒汉式
public class Singleton5 implements Serializable {
    private Singleton5() {
        System.out.println("private Singleton5()");
    }

    private static class Holder {
        static Singleton5 INSTANCE = new Singleton5();
    }

    public static Singleton5 getINSTANCE() {
        return Holder.INSTANCE;
    }

    public static void otherMethod() {
        System.out.println("otherMethod()");
    }
}
