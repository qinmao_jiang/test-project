package base;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-05-05 15:06:37
 */

//单例模式 - 枚举饿汉式
public enum Singleton2 {
    INSTANCE;

    private Singleton2() {
        System.out.println("private Singleton2()");
    }

    public static Singleton2 getINSTANCE() {
        return INSTANCE;
    }

    public static void otherMethod() {
        System.out.println("otherMethod()");
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(hashCode());
    }
}
