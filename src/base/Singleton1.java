package base;

import java.io.Serializable;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-05-05 14:44:04
 */

//单例模式 - 饿汉式
public class Singleton1 implements Serializable {
    private Singleton1() {
        if (INSTANCE != null) {
            throw new RuntimeException("单例对象不可重复被创建！");
        }
        System.out.println("private Singleton1()");
    }

    private static final Singleton1 INSTANCE = new Singleton1();

    public static Singleton1 getINSTANCE() {
        return INSTANCE;
    }

    public static void otherMethod() {
        System.out.println("otherMethod()");
    }
}
