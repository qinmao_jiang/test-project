package base;

import java.io.Serializable;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-05-05 15:15:43
 */

//单例模式 - 双检锁懒汉式
public class Singleton4 implements Serializable {
    private Singleton4() {
        System.out.println("private Singleton4()");
    }

    private static volatile Singleton4 INSTANCE = null;

    public static Singleton4 getINSTANCE() {
        if (INSTANCE == null) {
            synchronized (Singleton4.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Singleton4();
                }
            }
        }
        return INSTANCE;
    }

    public static void otherMethod() {
        System.out.println("otherMethod()");
    }
}
