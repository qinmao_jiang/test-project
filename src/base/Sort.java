package base;

import java.util.Arrays;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-05-04 15:57:54
 */
public class Sort {
    public static void main(String[] args) {
        int[] nums = {2, 5, 6, 1, 7, 10, 8, 3};
        quickSort(nums, 0, nums.length - 1);
        System.out.println(Arrays.toString(nums));
        Arrays.sort(nums);
    }

    /**
     * 二分查找 - 对应于JDK中 Arrays.binarySearch
     * @param nums   有序数组
     * @param target 查找目标值
     * @return 目标值的下标
     */
    private static int binarySearch(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;

        while (left <= right) {
            int mid = (left + right) / 2;   //可作出如下优化，解决溢出
//            int mid = left + (right - left) / 2;
//            int mid = (left + right) >>> 1;

            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return -1;
    }

    /**
     * 冒泡排序
     * @param nums 需排序数组
     */
    private static void bubbleSort1(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            boolean isSwap = true;
            for (int j = 0; j < nums.length - i - 1; j++) {
                if (nums[j] > nums[j + 1]) {
                    swap(nums, j, j + 1);
                    isSwap = false;
                }
            }

            if (isSwap) {   //优化，如果某一轮冒泡没有发生交换，则说明已有序，故可退出
                break;
            }
        }
    }

    private static void bubbleSort2(int[] nums) {
        int n = nums.length - 1;

        do {
            int last = 0;
            for (int i = 0; i < n; i++) {
                if (nums[i] > nums[i + 1]) {
                    swap(nums, i, i + 1);
                    last = i;
                }
            }
            n = last;
        } while (n != 0);   //每轮冒泡时，最后一次交换索引可以作为下一轮冒泡的比较次数
    }

    /**
     * 选择排序
     * @param nums 需排序数组
     */
    private static void selectionSort(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[min] > nums[j]) {
                    min = j;
                }
            }

            if (min != i) { //优化，为减少交换次数，每一轮可先找到最小的索引，在最后进行元素交换
                swap(nums, min, i);
            }
        }
    }

    /**
     * 插入排序
     * @param nums 需排序数组
     */
    private static void insertSort(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            int temp = nums[i];
            int j = i - 1;
            while (j >= 0) {
                if (temp < nums[j]) {
                    nums[j + 1] = nums[j];
                } else {
                    break;  //退出循环
                }
                j--;
            }
            nums[j + 1] = temp;
        }
    }

    /**
     * 希尔排序
     * @param nums 需排序数组
     */
    private static void shellSort(int[] nums) {
        int length = nums.length;
        for (int gap = length / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < length; i++) {
                int temp = nums[i];
                int j = i;
                while (j >= gap) {
                    if (temp < nums[j - gap]) {
                        nums[j] = nums[j - gap];
                        j -= gap;
                    } else {
                        break;
                    }
                }
                nums[j] = temp;
            }
        }
    }

    /**
     * 快速排序（单边/双边快排）
     * @param nums 需排序数组
     */
    private static void quickSort(int[] nums, int low, int high) {
        if (low >= high) {
            return;
        }

//        int p = partition1(nums, low, high);
        int p = partition2(nums, low, high);
        quickSort(nums, low, p - 1);
        quickSort(nums, p + 1, high);
    }

    /**
     * 单边快排分区
     * @param nums  需排序数组
     * @param low   数组最小下标
     * @param high  数组最大下标
     * @return  分区分解点下标
     */
    private static int partition1(int[] nums, int low, int high) {
        int pv = nums[high];
        int index = low;
        for (int i = low; i < high; i++) {
            if (nums[i] < pv) {
                if (i != index) {   //优化
                    swap(nums, i, index);
                }
                index++;
            }
        }
        if (index != high) {    //优化
            swap(nums, index, high);
        }
        return index;
    }

    /**
     * 双边快排分区
     * @param nums  需排序数组
     * @param low   数组最小下标
     * @param high  数组最大下标
     * @return  分区分解点下标
     */
    private static int partition2(int[] nums, int low, int high) {
        int pv = nums[low];
        int indexLeft = low;
        int indexRight = high;
        while (indexLeft < indexRight) {
            while (indexLeft < indexRight && nums[indexRight] > pv) {
                indexRight--;
            }

            while (indexLeft < indexRight && nums[indexLeft] <= pv) {
                indexLeft++;
            }

            swap(nums, indexLeft, indexRight);
        }

        swap(nums, low, indexRight);
        return indexRight;
    }

    /**
     * 数组元素交换
     * @param nums 数组
     * @param i    交换元素下标
     * @param j    交换元素下标
     */
    private static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}


