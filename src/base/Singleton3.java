package base;

import java.io.Serializable;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-05-05 15:15:43
 */

//单例模式 - 懒汉式
public class Singleton3 implements Serializable {
    private Singleton3() {
        System.out.println("private Singleton3()");
    }

    private static Singleton3 INSTANCE = null;

    public static synchronized Singleton3 getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new Singleton3();
        }
        return INSTANCE;
    }

    public static void otherMethod() {
        System.out.println("otherMethod()");
    }
}
