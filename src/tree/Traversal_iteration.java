package tree;

import java.util.*;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-10 21:18:02
 * @description Traversal_iteration类
 */
public class Traversal_iteration {
    public static void main(String[] args) {
        TreeNode node2 = new TreeNode(3, null, null);
        TreeNode node1 = new TreeNode(2, node2, null);
        TreeNode root = new TreeNode(1, null, node1);

        ArrayList<Integer> list = new ArrayList<>();
        // preOrder(root, list);
        // postOrder(root, list);
        inOrder(root, list);
        System.out.println(list);

    }

    //前序遍历
    private static void preOrder(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode pop = stack.pop();
            result.add(pop.value);

            if (pop.right != null) {
                stack.push(pop.right);
            }

            if (pop.left != null) {
                stack.push(pop.left);
            }
        }
    }

    //后序遍历
    private static void postOrder(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode pop = stack.pop();

            if (pop.right != null) {
                stack.push(pop.right);
            }

            if (pop.left != null) {
                stack.push(pop.left);
            }

            result.add(pop.value);
            result.sort(Comparator.reverseOrder());
        }
    }

    //中序遍历
    private static void inOrder(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        Stack<TreeNode> stack = new Stack<>();
        TreeNode current = root;
        while (current != null || !stack.isEmpty()) {
            if (current != null) {
                stack.push(current);
                current = current.left;
            } else {
                current = stack.pop();
                result.add(current.value);
                current = current.right;
            }
        }
    }
}
