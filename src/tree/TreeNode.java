package tree;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-10 20:10:33
 * @description TreeNode类
 */
public class TreeNode {
    int value;
    TreeNode left;
    TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int value) {
        this.value = value;
    }

    public TreeNode(int value, TreeNode left, TreeNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }
}
