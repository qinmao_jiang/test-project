package tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-10 20:15:38
 * @description PreOrder类
 */
public class Traversal_backTrack {
    public static void main(String[] args) {
        TreeNode node2 = new TreeNode(3, null, null);
        TreeNode node1 = new TreeNode(2, node2, null);
        TreeNode root = new TreeNode(1, null, node1);

        ArrayList<Integer> list = new ArrayList<>();
        // preOrder(root, list);
        // inOrder(root, list);
        postOrder(root, list);
        System.out.println(list);

    }

    //二叉树的前序遍历
    public static void preOrder(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        result.add(root.value);
        preOrder(root.left, result);
        preOrder(root.right, result);
    }

    //二叉树的中序遍历
    public static void inOrder(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        inOrder(root.left, result);
        result.add(root.value);
        inOrder(root.right, result);
    }

    //二叉树的后序遍历
    public static void postOrder(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        postOrder(root.left, result);
        postOrder(root.right, result);
        result.add(root.value);
    }
}
