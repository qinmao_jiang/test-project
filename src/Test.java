import base.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author 江子彧
 * @version 1.0
 * @date 2023-05-05 09:47:08
 */
public class Test {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Singleton5.otherMethod();
        System.out.println("=======================");
        System.out.println(Singleton5.getINSTANCE());
        System.out.println(Singleton5.getINSTANCE());

        //反射破坏单例
        reflection(Singleton5.class);
    }

    private static void reflection(Class<?> clazz) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
//        for (Constructor<?> constructor : clazz.getConstructors()) {
//            System.out.println(constructor);
//        }

        Constructor<?> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        System.out.println("反射创建实例：" + constructor.newInstance());
    }
}
