package annotation;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-06 16:50:00
 * @description Demo类
 */
@MyTest(value = "jqm", num = 10, str = {"南昌", "广州"})
public class Demo {
    @MyTest(value = "jzy", num = 5, str = {"九江", "都昌"})
    public void test() {
        System.out.println("test...");
    }
}
