package annotation;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-06 16:56:01
 * @description AnnotationTest类
 */
public class AnnotationTest {
    @Test
    public void parseClass() {
        Class<Demo> clazz = Demo.class;
        if (clazz.isAnnotationPresent(MyTest.class)) {
            MyTest myTest = clazz.getDeclaredAnnotation(MyTest.class);
            System.out.println(myTest.value());
            System.out.println(myTest.num());
            System.out.println(Arrays.toString(myTest.str()));
        }
    }
    
    @Test
    public void parseMethod() throws NoSuchMethodException {
        Class<Demo> clazz = Demo.class;
        Method test = clazz.getMethod("test");
        if (test.isAnnotationPresent(MyTest.class)) {
            MyTest myTest = test.getDeclaredAnnotation(MyTest.class);
            System.out.println(myTest.value());
            System.out.println(myTest.num());
            System.out.println(Arrays.toString(myTest.str()));
        }
    }
}
