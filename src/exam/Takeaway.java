package exam;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author JQM
 * @version 1.0
 * @date 2023-08-20 21:24:41
 * @description Takeaway类
 */
public class Takeaway {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();

        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("HH:mm");
        for (int i = 0; i < t; i++) {
            LocalTime t1 = LocalTime.parse(scanner.next(), pattern);
            LocalTime t2 = LocalTime.parse(scanner.next(), pattern);
            LocalTime t3 = LocalTime.parse(scanner.next(), pattern);
            if (t3.isAfter(t2)) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
