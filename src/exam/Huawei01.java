package exam;

import java.util.Scanner;

/**
 * @author JQM
 * @version 1.0
 * @date 2023-10-01 09:42:29
 * @description 华为第一题
 *              某商城进行 ”双十一“ 促销活动，活动采用等价格减免的方式，某位客人一次购买了 N 件商品，需要帮忙计算本次购买能获得的总优惠。
 *              给定商品价格数组 p，其中 p[i] 表示第 i 件商品的价格，第 i 件商品能获得的优惠为第 i 件商品之前的第 j 件商品的价格，其中 p[j] <= p[i]，
 *              并且 j < i，且 P[j] 是离 p[i] 最近的一个小于等于 p[i] 的商品。求本次购买能获得的总优惠。
 *                  ->第一行是商品的个数N，1 <= N <= 100000;
 *                  ->第二行是用空格分隔的N个整数，数组元素的值表示商品的价格 0 < p[i] < 100000;
 *                  ->输出为一个整数，表示本次购买获得的总优惠
 *              样例：输入：5
 *                        9 4 5 2 4
 *                   输出：6
 */
public class Huawei01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] prices = new int[n];
        long result = 0;
        for (int i = 0; i < n; i++) {
            prices[i] = scanner.nextInt();
            int index = i - 1;
            while (index >= 0 && prices[i] < prices[index]) {
                index--;
            }

            if (index >= 0) {
                result += prices[index];
            }
        }
        System.out.println(result);
    }
}
