package exam;

import java.util.LinkedList;
import java.util.List;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-08 14:59:09
 * @description Palindrome类
 */
public class Palindrome {
    public static void main(String[] args) {
        String s = "aab";
        List<List<String>> result = partition(s);
        System.out.println(result);
    }

    public static List<List<String>> partition(String s) {
        List<List<String>> res = new LinkedList<>();
        List<String> path = new LinkedList<>();
        backTacking(s, 0, res, path);
        return res;
    }

    private static void backTacking(String s, int index, List<List<String>> res, List<String> path) {
        if (index >= s.length()) {
            res.add(new LinkedList<>(path));
            return;
        }

        for (int i = index; i < s.length(); i++) {
            if (isPalindrome(s, index, i)) {
                path.add(s.substring(index, i + 1));
            } else {
                continue;
            }

            backTacking(s, i + 1, res, path);
            path.remove(path.size() - 1);
        }
    }

    private static boolean isPalindrome(String s, int start, int end) {
        for (int i = start, j = end; i < j; i++, j--) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
        }
        return true;
    }
}
