package exam;

import java.util.Scanner;

/**
 * @author JQM
 * @version 1.0
 * @date 2023-08-20 20:05:41
 * @description MaxValue类
 *              小美有一个长度为n的数组，她想将这个数组进行求和，即 sum=a1+a2+...+an。
 *              小美可以使用一次魔法（也可以不使用），将其中一个加号变成乘号，使得sum最大。
 *              输入描述：第一行输入一个整数n。第二行输入n个整数表示数组a。
 *              输入：1
 *                   1 1 4 5 1 4
 *              输出：27
 */
public class MaxValue {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += arr[i];
        }

        int res = sum;
        for (int i = 0; i < n - 1; i++) {
            int temp = arr[i] * arr[i + 1];
            temp = sum + temp - arr[i] - arr[i + 1];
            if (temp > res) {
                res = temp;
            }
        }

        System.out.println(res);
    }
}
