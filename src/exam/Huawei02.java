package exam;

import java.util.Scanner;
import java.util.StringJoiner;

/**
 * @author JQM
 * @version 1.0
 * @date 2023-10-03 16:29:01
 * @description 华为第二题
 *              给定两个一元多项式以及多项式运算符，计算输出两个多项式运算的结果，计算规则见样例。
 *                  -> 分三行输入，第一行输入多项式A的系数数组(按照阶数高到低顺序)，
 *                     第二行输入多项式B的系数数组，第三行输入多项式运算符运算符包括加(+)减(-)乘()三种，
 *                     系数数组大小小于128，系数取值范围[-512 512]。
 *                  -> 输出多项式运算结果的系数数组，如果计算后多项式为0，则输出0，从第1个非零的系数开始输出。
 *              样例：输入：[1 2 3 4 5 6]
 *                        [2 3 -4]
 *                        +
 *                   输出：[1 2 3 6 8 2]
 */
public class Huawei02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line1 = scanner.nextLine();
        line1 = line1.substring(1, line1.length() - 1);
        String line2 = scanner.nextLine();
        line2 = line2.substring(1, line2.length() - 1);
        String sign = scanner.nextLine();

        String[] split1 = line1.split("\\s+");
        int[] a = new int[split1.length];
        for (int i = 0; i < split1.length; i++) {
            a[i] = Integer.parseInt(split1[i]);
        }

        String[] split2 = line2.split("\\s+");
        int[] b = new int[split2.length];
        for (int i = 0; i < split2.length; i++) {
            b[i] = Integer.parseInt(split2[i]);
        }

        int[] res = calculator(a, b, sign);
        //去除高位的0
        int index = 0;
        while (res[index] == 0) {
            index++;
        }

        StringJoiner joiner = new StringJoiner(" ", "[", "]");
        for (int s : res) {
            joiner.add(String.valueOf(s));
        }
        System.out.println(joiner);
    }

    private static int[] calculator(int[] a, int[] b, String sign) {
        int aLength = a.length;
        int bLength = b.length;
        if ("+".equals(sign) || "-".equals(sign)) {
            int maxLength = Math.max(aLength, bLength);
            int[] res = new int[maxLength];
            int[] a1 = new int[maxLength];
            int[] b1 = new int[maxLength];
            System.arraycopy(a, 0, a1,maxLength - a.length, a.length );
            System.arraycopy(b, 0, b1,maxLength - b.length, b.length );

            switch (sign) {
                case "+" :
                    for (int i = 0; i < maxLength; i++) {
                        res[i] = a1[i] + b1[i];
                    }
                    break;
                case "-" :
                    for (int i = 0; i < maxLength; i++) {
                        res[i] = a1[i] - b1[i];
                    }
                    break;
                default:
                    break;
            }
            return res;
        } else if ("*".equals(sign)) {
            int[] res = new int[aLength + bLength - 1];

            for (int i = 0; i < aLength; i++) {
                for (int j = 0; j < bLength; j++) {
                    res[i + j] += a[i] * b[j];
                }
            }
            return res;
        } else {
            return new int[]{0};
        }
    }
}
