package exam;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-08 14:15:45
 * @description 组合总和（回溯算法）
 */
public class Sum {
    public static void main(String[] args) {
        int[] candidates = {10, 1, 2, 7, 6, 1, 5};
        int target = 8;
        List<List<Integer>> result = combinationSum(candidates, target);
        System.out.println(result);
    }

    private static List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> res = new LinkedList<>();
        List<Integer> path = new LinkedList<>();
        int sum = 0;
        // int[] used = new int[candidates.length];
        Arrays.sort(candidates);
        // backTracking1(candidates, target, 0, res, path, sum, used);
        backTracking2(candidates, target, 0, res, path, sum);
        return res;
    }

    private static void backTracking1(int[] candidates, int target, int index, List<List<Integer>> res, List<Integer> path, int sum, int[] used) {
        if (sum == target) {
            res.add(new LinkedList<>(path));
            return;
        }

        for (int i = index; i < candidates.length; i++) {
            if (sum + candidates[i] > target) {
                break;
            }

            if (i > 0 && candidates[i] == candidates[i - 1] && used[i - 1] == 0) {
                continue;
            }

            used[i] = 1;
            sum += candidates[i];
            path.add(candidates[i]);
            backTracking1(candidates, target, i + 1, res, path, sum, used);
            path.remove(path.size() - 1);
            sum -= candidates[i];
            used[i] = 0;
        }
    }

    private static void backTracking2(int[] candidates, int target, int index, List<List<Integer>> res, List<Integer> path, int sum) {
        if (sum == target) {
            res.add(new LinkedList<>(path));
            return;
        }

        for (int i = index; i < candidates.length && sum + candidates[i] <= target; i++) {
            if (i > index && candidates[i] == candidates[i - 1]) {
                continue;
            }

            sum += candidates[i];
            path.add(candidates[i]);
            backTracking2(candidates, target, i + 1, res, path, sum);
            path.remove(path.size() - 1);
            sum -= candidates[i];
        }
    }
}
