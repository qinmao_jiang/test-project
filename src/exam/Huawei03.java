package exam;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @version 1.0
 * @author JQM
 * @date 2023-10-03 20:43:59
 * @description 华为第三题
 *              有 m 件货物和 n 辆卡车，每辆卡车只能运送一件货物，卡车的载重量需要大于等于货物重量才能运输，
 *              另有X个载重为的拖斗，每辆卡车最多可以拖挂一个拖斗以提升载重量，共同运输一件更重的货物;
 *              请你返回最多可以运输多少件货物。
 *                  -> 第1行包含四个数字，分别为：m:货物数量；n:卡车数量；x:拖斗数量；y:拖斗载重
 *                  -> 第2行为货物的重量列表，以空格分隔;
 *                  -> 第3行为卡车的载重列表，以空格分隔.
 *                  -> 输出整数，最多可以运输货物的数量。
 *              样例：输入：5 5 1 5
 *                        9 5 9 8 5
 *                        1 6 2 6 4
 *                   输出：3
 */
public class Huawei03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        int[] weight = new int[m];
        for (int i = 0; i < m; i++) {
            weight[i] = scanner.nextInt();
        }

        int[] load = new int[n];
        for (int i = 0; i < n; i++) {
            load[i] = scanner.nextInt();
        }

        Arrays.sort(weight);
        Arrays.sort(load);

        int res = 0;
        for (int i = m - 1; i >= 0; i--) {
            if (res >= n) {
                break;
            }
            if (weight[i] <= load[i]) {
                res++;
            } else if (x > 0 && weight[i] <= load[i] + y) {
                res++;
                x--;
            }
        }
        System.out.println(res);
    }
}
