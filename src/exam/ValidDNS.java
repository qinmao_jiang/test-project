package exam;

import java.util.Scanner;

/**
 * @author JQM
 * @version 1.0
 * @date 2023-09-12 19:44:32
 * @description 验证一个字符串是否是合法的DNS域名。DNS域名的格式要求如下:
 *                  1)域名由一系列以点分隔的标签组成。每个标签最长可为 63个字节。域名的总长度不能超过 255字节，包括点。域名至少由2个标签组成。
 *                  2) 域名标签只能包含字符 a-z、A-Z、0-9 和 -(连字符)。不能在标签开头或结尾指定连字符。域名支持大小写，但是不区分大小写。
 */
public class ValidDNS {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        boolean isValid = isValidDNS(str);
        System.out.println(isValid);
    }

    private static boolean isValidDNS(String str) {
        if (str.length() > 255) {
            return false;
        }

        String[] labels = str.split("\\.");
        if (labels.length < 2) {
            return false;
        }

        for (String label : labels) {
            if (label.length() > 63) {
                return false;
            }

            if (!label.matches("^[a-zA-Z0-9]+(-[a-zA-Z0-9]+)*$")) {
                return false;
            }

            if (label.startsWith("-") || label.endsWith("-")) {
                return false;
            }
        }

        return true;
    }
}
