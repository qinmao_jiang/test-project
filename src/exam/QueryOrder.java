package exam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author JQM
 * @version 1.0
 * @date 2023-08-20 19:52:11
 * @description QueryOrder类
 *      美团商家的订单发起时，订单编号最开始从 1 开始，后续每发起一个订单，订单编号便在上一订单编号的基础上 +1。
 *      为了防止订单号过大，商家还可以设置一个编号上限m，当订单编号超过m时，将又从 1 开始编号。
 *      小美想知道，当订单编号上限为m时，第x个订单编号是多少？将有q次询问。
 *      输入描述：第一行输入一个整数q(1<=q<=50000)。接下来q行，每行两个整数m,x(11m,x110^9)。
 *              输入：4
 *                   2 3
 *                   4 17
 *                   8 2
 *                   4 4
 *              输出：1
 *                   2
 *                   2
 *                   4
 */
public class QueryOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int q = scanner.nextInt();

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < q; i++) {
            int m = scanner.nextInt();
            int x = scanner.nextInt();
            int res = x % m;
            if (res == 0) {
                list.add(m);
            } else {
                list.add(res);
            }
        }

        list.forEach(System.out::println);
    }
}
